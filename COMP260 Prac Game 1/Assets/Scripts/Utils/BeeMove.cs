﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour {

	private float speed = 4.0f;        // metres per second
	private float turnSpeed = 180.0f;  // degrees per second
	public Transform target;
	//public Transform target2;
	private Vector2 heading = Vector2.right; 
	public float minSpeed = 3.0f; 
	public float maxSpeed = 5.0f;
	public float minTurnSpeed = 180.0f; 
	public float maxTurnSpeed = 200.0f;


	public ParticleSystem explosionPrefab;

	void OnDestroy () {
		//create an explosion at the bee's current position
		ParticleSystem explosion = Instantiate(explosionPrefab);
		explosion.transform.position = transform.position;

		//destroy the particle system when it is over
		Destroy(explosion.gameObject, explosion.main.duration);
	}


	// Use this for initialization
	void Start () {

		//find a player object to be the target by type
		//PlayerMove player = (PlayerMove)FindObjectOfType (typeof(PlayerMove));
		//target = player.transform;



		//bee initially moves in random direction
		heading = Vector2.right;
		float angle = Random.value * 360;
		heading = heading.Rotate (angle);

		//set speed and turnSpeed randomly
		speed = Mathf.Lerp(minSpeed,maxSpeed,Random.value);
		turnSpeed = Mathf.Lerp (minTurnSpeed, maxTurnSpeed, Random.value);


	}


	void Update() {
		if (target == null) {
			PlayerMoveW3 player = FindObjectOfType<PlayerMoveW3> ();
			if (player != null) {
				target = player.gameObject.transform;
			}
		}

		//Vector2 closest = Vector2.zero;

		// get the vector from the bee to the target 
		Vector2 direction = target.position - transform.position;
		//Vector2 direction2 = target2.position - transform.position;

		Debug.Log (direction);
		//Debug.Log (direction2);
		//if (direction.magnitude < direction2.magnitude) {
		//	closest = direction;
		//} else {
		//	closest = direction2;
		//}

		// calculate how much to turn per frame
		float angle = turnSpeed * Time.deltaTime;

		// turn left or right
		if (direction.IsOnLeft(heading)) {
			// target on left, rotate anticlockwise
			heading = heading.Rotate(angle);
		}
		else {
			// target on right, rotate clockwise
			heading = heading.Rotate(-angle);
		}
			
		transform.Translate(heading * speed * Time.deltaTime);
	}


	void OnDrawGizmos() {
		// draw heading vector in red
		Gizmos.color = Color.red;
		Gizmos.DrawRay(transform.position, heading);

		// draw target vector in yellow
		Gizmos.color = Color.yellow;
		Vector2 direction = target.position - transform.position;
		Gizmos.DrawRay(transform.position, direction);
	}
		
}
