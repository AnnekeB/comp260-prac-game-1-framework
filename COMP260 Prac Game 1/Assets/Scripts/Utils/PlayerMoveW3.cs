﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoveW3 : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

	public float maxSpeed = 5.0f; // in metres per second
	public float acceleration = 2.0f; // in metres/second/second
	private float speed = 0.0f;    // in metres/second
	public float brake = 15.0f; // in metres/second/second
	public float turnSpeed = 70.0f; // in degrees/second
	private beeSpawner beeSpawner;
	public float destroyRadius = 1.0f;


	void Update ()	{
		//fnid the bee spawner and store a reference for later
		beeSpawner = FindObjectOfType<beeSpawner> ();

		if (Input.GetButtonDown ("Fire1")) {
			//destroy nearby bees
			beeSpawner.DestroyBees (transform.position, destroyRadius);
		}

			// the horizontal axis controls the turn
			float turn = Input.GetAxis ("Horizontal");

			// turn the cat
			transform.Rotate (0, 0, turn * turnSpeed * Time.deltaTime * (1 + (speed / maxSpeed)));

			// the vertical axis controls acceleration fwd/back
			float forwards = Input.GetAxis ("Vertical");
			if (forwards > 0) {
				// accelerate forwards
				speed = speed + acceleration * Time.deltaTime;
			} else if (forwards < 0) {
				// accelerate backwards
				speed = speed - acceleration * Time.deltaTime;
			} else {
				// braking
				if (speed > 0) {
					speed = Mathf.Max (speed - brake * Time.deltaTime, 0);
				} else if (speed < 0) {
					speed = Mathf.Min (speed + brake * Time.deltaTime, 0);
				}
			}


			// clamp the speed
			speed = Mathf.Clamp (speed, -maxSpeed, maxSpeed);

			// compute a vector in the up direction of length speed
			Vector2 velocity = Vector2.up * speed;

			// move the object
			transform.Translate (velocity * Time.deltaTime, Space.Self);

		}

	}

