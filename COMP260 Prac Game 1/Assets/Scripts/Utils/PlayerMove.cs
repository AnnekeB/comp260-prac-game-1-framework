﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {
	public Vector2 move;
	public Vector2 velocity; //m/s
	public float maxSpeed = 5.0f;
	public string horizontalInputAxis;
	public string verticalInputAxis;


	void Update () {
		//get the input values
		Vector2 direction;

		direction.x = Input.GetAxis (horizontalInputAxis);
		direction.y = Input.GetAxis (verticalInputAxis);

		//scale by the maxSpeed param
		Vector2 velocity = direction * maxSpeed;
		//scale the velocity by the frame duration
		Vector2 move = velocity * Time.deltaTime;

		//move the object
		transform.Translate (move);

		//move the object
		transform.Translate(velocity*Time.deltaTime);

		
	}
}
