﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class beeSpawner : MonoBehaviour {
	public PlayerMoveW3 playerPrefab;
	//public PlayerMove player;
	public int nBees = 20;
	public BeeMove beePrefab;
	public Rect spawnRect;
	public float xMin, yMin;
	public float width, height;
	public float minBeePeriod;
	public float maxBeePeriod;

	void Start () {
		StartCoroutine (SpawnBees ());

	}

	IEnumerator SpawnBees (){
		//instantiate player
		PlayerMoveW3 player = Instantiate(playerPrefab);

		yield return new WaitForSeconds (Random.Range (minBeePeriod, maxBeePeriod));
		//create bees
		for (int i = 0; i < nBees; i++) {
			BeeMove bee = Instantiate (beePrefab);

			//set the target
			//bee.target = player.transform;

			//attach to this object in the heirarchy
			bee.transform.parent = transform;
			//give name and number
			bee.gameObject.name = "Bee" + i;

			//move the bee to a random pos within the spawn rect
			float x = spawnRect.xMin + Random.value * spawnRect.width;
			float y = spawnRect.yMin + Random.value * spawnRect.height;

			bee.transform.position = new Vector2 (x, y);
			Debug.Log (bee.gameObject.name);
		}

	}
		public void DestroyBees(Vector2 centre, float radius) {
		// destroy all bees within ‘radius’ of ‘centre’
		for (int i = 0; i < transform.childCount; i++) {
			Transform child = transform.GetChild(i);
			 
			Vector2 v = (Vector2)child.position - centre;
			if (v.magnitude <= radius) {
				Destroy(child.gameObject);
			}
		}
	}

	void Update () {
		
	}

	void OnDrawGizmos() {
		// draw the spawning rectangle
		Gizmos.color = Color.green;
		Gizmos.DrawLine(
			new Vector2(spawnRect.xMin, spawnRect.yMin), 
			new Vector2(spawnRect.xMax, spawnRect.yMin));
		Gizmos.DrawLine(
			new Vector2(spawnRect.xMax, spawnRect.yMin), 
			new Vector2(spawnRect.xMax, spawnRect.yMax));
		Gizmos.DrawLine(
			new Vector2(spawnRect.xMax, spawnRect.yMax), 
			new Vector2(spawnRect.xMin, spawnRect.yMax));
		Gizmos.DrawLine(
			new Vector2(spawnRect.xMin, spawnRect.yMax), 
			new Vector2(spawnRect.xMin, spawnRect.yMin));
	}



}